
ifconfig eth0 192.168.157.132 netmask 255.255.255.0


1、切换到网卡配置文件目录
```shell
cd /etc/sysconfig/network-scripts/
```

2、编辑网卡配置文件 ， BOOTPROTO改为static，ONBOOT改为yes，添加静态IP：IPADDR, 子网掩码：NETMASK, 网关：GATEWAY，vm编辑=》虚拟网络编辑器=》nat设置可以查看子网ip，子网掩码，网关；子网掩码和网关配置一致，ipaddr（192.168.157.）前三位和子网IP一致，后一位0~255。
``` shell
vi ifcfg-ens33
```

```ifcfg-ens33
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static        #改为static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=ens33
UUID=d11eb958-20b8-4e89-b9ed-49e9ad2c1995
DEVICE=ens33
ONBOOT=yes              #改为yes
IPADDR=192.168.157.116  #静态IP
NETMASK=255.255.255.0   #子网掩码
GATEWAY=192.168.157.2   #网关
```

3、重启网络服务
```shell
systemctl restart network
```

