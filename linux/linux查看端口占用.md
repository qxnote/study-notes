1、查看所有端口占用情况
```shell
netstat -anp

# 或者

netstat -tln
```

2、查看单个端口占用情况

```shell
netstat -nap | grep 8080

# 或者

netstat -tln | grep 8080
```