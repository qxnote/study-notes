事务的作用：在数据层保障一系列的数据库操作同成功同失败
Spring事务的作用：在数据层或业务层保障一系列数据库操作同成功同失败

```java
public interface PlatfromTransactionManager {
	void commit(TransactionStatus status) throws TransactionException;
	void rollback(TransactionStatus status) throw TransactionException;
}

public class DataSourceTransactionManager {
	......
}
```

案例：银行账户转账

模拟银行账户间转账业务
需求： 实现任意两个账户间转账操作
需求微缩：A账户减钱，B账户加钱

分析：
1.  数据层提供基础操作，指定账户减钱 ( outMoney )，指定账户加钱 ( inMoney )
2.  业务层提供转账操作 ( transfer ), 调用减钱与加钱操作
3.  提供两个账户和操作金额执行转账操作
4.  基于Spring整合MyBatis环境搭建上述操作

结果分析：
1.  程序正常执行时，账户金额A减B加，没有问题
2.  程序出现异常后，转账失败，但是异常之前操作成功，异常之后操作失败，整体业务失败

```java
//1、EnableTransactionManagement注解告知Spring开启事务驱动
@Configuration  
@ComponentScan("com.qxiao")  
@PropertySource("classpath:jdbc.properties")  
@Import({JdbcConfig.class, MybatisConfig.class})  
@EnableAspectJAutoProxy  
@EnableTransactionManagement  
public class SpringConfig {  
}

//2、新增transactionManager事务管理bean
@Bean  
public PlatformTransactionManager transactionManager (DataSource dataSource) {  
    DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();  
    transactionManager.setDataSource(dataSource);  
    return transactionManager;  
}

// 3、Service接口添加Transactional事务注解
public interface AccountService {  

    @Transactional  
    public void transfer(String out, String in, Double money);  
}
```

*Spring注解式事务通常添加在**业务层接口**中而不会添加到业务层实现类中，降低耦合
*注解式事务可以添加到业务方法上表示当前方法开启事务，也可以添加到接口上表示当前接口所有方法开启事务*


## Spring事务角色

- 事务管理员：发起事务方，在Spring中通常指代业务层开启事务的方法
- 事务协调员：加入事务方，在Spring中通常指代数据层方法，也可以是业务层方法

## 事务相关配置

|属性|作用|示例|
|----|----|----|
| readOnly| 设置是否为只读事务| readOnly=true 只读事务|
|timeout|设置事务超时时间|timeout=-1 ( 永不超时 )|
|rollbackFor|设置事务回滚异常(class)|rollbackFro={NullPointException.class}|
|rollbackForClassName|设置事务回滚异常(String)|同上格式为字符串|
|noRollbackFor|设置事务不回滚异常(class)|noRollbackFro={NullPointException.class}|
|noRollBackForClassName|设置事务不回滚异常(String)|同上格式为字符串|
|propagation|设置事务传播行为| ......|

```java
public interface AccountService {  

	// IOException异常默认不回滚，手动添加回滚rollbackFor = {IOException.class}
	// 让IOException异常时也进行回滚
    @Transactional(rollbackFor = {IOException.class})  
    public void transfer(String out, String in, Double money) throws IOException;  
}

@Service  
public class AccountServiceImpl implements AccountService {  
  
    @Autowired  
    private AccountDao accountDao;  
  
    public void transfer(String out, String in, Double money) throws IOException {  
        accountDao.outMoney(out, money);  
        if(true) throw new IOException(); // 测试抛出 IOException异常
        accountDao.inMoney(in, money);  
    }  
}
```

案例：转账业务追加日志

需求：实现任意两个账户间转账操作，并对每次转账操作在数据库进行留痕
需求微缩：A账户减钱，B账户加钱，数据库记录日志

分析：
1.  基于转账操作案例添加日志模块，实现数据库中记录日志
2.  业务层转账操作 (transfer), 调用减钱、加钱与记录日志功能

实现效果预期：
 -  无论转账操作是否成功，均进行转账操作的日志留痕

```java
public interface LogDao {  
  
    @Insert("INSERT INTO log (info, create_time) VALUES(#{info}, now())")  
    void log (String info);  
}

public interface LogService {  
  
    @Transactional  
    void log(String out, String in, Double money);  
}

@Service  
public class LogServiceImpl implements LogService {  
  
    @Autowired  
    private LogDao logDao;  
  
    public void log(String out, String in, Double money) {  
        logDao.log("转账操作由" + out + "到" + in + ", 金额：" + money);  
    }  
}


@Service  
public class AccountServiceImpl implements AccountService {  
  
    @Autowired  
    private AccountDao accountDao;  
  
    @Autowired  
    private LogService logService;  
  
    public void transfer(String out, String in, Double money) { 

		// try finally 保障转账是否异常 log都会执行
        try {  
            accountDao.outMoney(out, money);  
            accountDao.inMoney(in, money);  
        } finally {  
            logService.log(out, in, money);  
        }  
    }  
}
```

存在的问题： 
	日志的记录与转账操作隶属于同一个事务，同成功同失败
实现效果改进：
	无论转账是否成功，日志必须保留

```java
public interface LogService {  
    // 设置事务传播行为REQUIRES_NEW ( 需要新事务 )
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void log(String out, String in, Double money);  
}
```

### 事务传播行为

 事务传播行为: 事务协调员对事务管理员所携带事务的处理态度

![[Pasted image 20230311152320.png]]

