```java
// 获取bean三种方式
BookDao bookDao = (BookDao) ctx.getBean("bookDao");

BookDao bookDao = ctx.getBean("bookDao", BookDao.class);

BookDao bookDao = ctx.getBean(BookDao.class);
```