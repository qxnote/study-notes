思考：向一个类传递数据的方式有几种？
-  普通方法( setter )
-  构造方法

思考：依赖注入描述了在容器中建立bean与bean之间依赖关系的过程，如果bean运行需要的是数字或字符串呢？
-  引用类型
-  简单类型 ( 基本数据类型与String )

依赖注入方式
-  setter 注入
  1.  简单类型
  2.  引用类型
-  构造器注入
  1.  简单类型
  2.  引用类型

一、setter 注入引用类型
```java
public class BookServiceImpl implements BookService {  
    private BookDao bookServiceDao;  
    
	//  setter 注入 -> 引用类型
    public void setBookServiceDao(BookDao bookServiceDao) {  
        this.bookServiceDao = bookServiceDao;  
    }  
  
    public void save() {  
        System.out.println("book service save ...");  
        bookServiceDao.save();  
    }  
}
```

```xml
<!-- 方式一：使用构造方法实例化bean   -->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl"></bean>

<bean id="bookService" class="org.qxnote.service.impl.BookServiceImpl">  
    <!-- 配置Service与Dao的关系 -->  
    <property name="bookServiceDao" ref="bookDao"/>  
</bean>
```

二、setter 注入简单类型
```java
public class BookDaoImpl implements BookDao {  
  
    private int connectionNum;  
    private String databaseName;  
  
    public void setConnectionNum(int connectionNum) {  
        this.connectionNum = connectionNum;  
    }  
  
    public void setDatabaseName(String databaseName) {  
        this.databaseName = databaseName;  
    }  
  
    public void save() {  
        System.out.println("book dao save ..." + databaseName + "," + connectionNum);
        // book dao save ...mysql,10  
    }  
}
```

```xml
<!-- 方式一：使用构造方法实例化bean   -->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl">  
    <property name="connectionNum" value="10" />  
    <property name="databaseName" value="mysql" />  
</bean>
```

三、构造器注入引用类型
```java
public class BookDaoImpl implements BookDao {  
    public void save() {  
        System.out.println("book dao save ...");  
    }
}

public class BookServiceImpl implements BookService {  
    private BookDao bookServiceDao;  
  
    public BookServiceImpl(BookDao bookServiceDao) {  
        this.bookServiceDao = bookServiceDao;  
    }  
  
    public void save() {  
        System.out.println("book service save ...");  
        bookServiceDao.save();  
    }  
}
```

```xml
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl"></bean>

<bean id="bookService" class="org.qxnote.service.impl.BookServiceImpl">  
    <!-- 配置Service与Dao的关系 -->  
    <constructor-arg name="bookServiceDao" ref="bookDao"/>  
</bean>
```

四、构造器注入简单类型
```java
public class BookDaoImpl implements BookDao {  
    private int connectionNum;  
    private String databaseName;  
  
    public BookDaoImpl(int connectionNum, String databaseName) {  
        this.connectionNum = connectionNum;  
        this.databaseName = databaseName;  
    }  
  
    public void save() {  
        System.out.println("book dao save ..." + databaseName + "," + connectionNum);  
    }
}
```

```xml
<!-- 标准配置-->
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl">  
    <constructor-arg name="connectionNum" value="10"/>  
    <constructor-arg name="databaseName" value="mysql"/>  
</bean>  
  
<!-- 解决形参名称问题，与形参名不耦合-->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl">  
    <constructor-arg type="int" value="10"/>  
    <constructor-arg type="java.lang.String" value="mysql"/>  
</bean>  
  
<!-- 解决参数重复类型问题，使用位置解决参数匹配-->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl">  
    <constructor-arg index="1" value="mysql"/>  
    <constructor-arg index="0" value="10"/>  
</bean>
```

## 依赖注入方式选择

1.  强制依赖使用构造器进行，使用setter注入有概率不进行注入导致null对象出现
2.  可选依赖使用setter注入进行，灵活性强
3.  Spring框架倡导使用构造器，第三方框架内部大多数采用构造器注入的形式进行数据初始化，相对严谨
4.  如果有必要可以两者同时使用，使用构造器注入完成强制依赖的注入，使用setter完成可选依赖的注入
5.  实际开发过程中还要根据实际情况分析，如果受控对象没有提供setter方法就必须使用构造器注入
6.  自己开发的模块推荐使用setter注入