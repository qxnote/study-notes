
AOP ( Aspect Oriented Programming ) 面向切面编程，一种编程范式，指导开发者如何组织程序结构 
 -  OOP ( Object Oriented Programming ) 面向对象编程

作用：在不改动原始设计的基础上为其进行功能增强

Spring理念：无入侵式编程


##  AOP核心概念
-  连接点 ( JoinPoint ) :  程序执行过程中的任意位置，粒度为执行方法、抛出异常、设置变量等
   1.  在SpringAOP中， 理解为方法的执行
-  切入点 ( Pointcut ) : 匹配链接点的式子
   1.   在SpringAOP中， 一个切入点可以只描述一个具体方法，也可以匹配多个方法
       -  一个具体方法： com.qxiao.dao包下的BookDao接口中的无形参无返回值的save方法
       -  匹配多个方法： 所有的save方法， 所有的get开头的方法， 所欲以Dao结尾的接口中的任意方法， 所有带一个参数的方法
-  通知 ( Advice ) :  在切入点处执行的操作，也就是共性功能
   1.   在SpringAOP中， 功能最终以方法的形式呈现
-  通知类： 定义通知的类
-  切面 ( Aspect ) :  描述通知与切入点的对应关系
-  目标对象 ( Target ) :  原始功能去掉共性功能对应的类产生的对象，这种对象是无法直接完成最终工作的
-  代理 ( Proxy ) :  目标对象无法直接完成工作，需要对其进行功能回填，通过原始对象的代理对象实现

## AOP入门案例思路分析
案列设定： 测试接口执行效率
简化设定： 在接口执行前输出当前系统时间
开发模式： XML or 注解
思路分析： 
 1.  导入坐标
 2.  制作连接点方法 ( 原始操作，Dao接口与实现类 )
 3.  制作共性功能 ( 通知类与通知 )
 4.  定义切入点
 5.  绑定切入点与通知关系 ( 切面 )

```xml
<!-- 导入坐标 -->
<dependency>  
    <groupId>org.aspectj</groupId>  
    <artifactId>aspectjweaver</artifactId>  
    <version>1.9.4</version>  
</dependency>
```

```java
@Component  // bean
@Aspect  // AOP
public class MyAdvice {

	// 定义切入点
	// 切入点定义依托一个不具有实际意义的方法进行，即无参数，无返回值，方法体无实际逻辑
    @Pointcut("execution(void com.qxiao.dao.BookDao.update())")  
    private void pt() {}  
  
    @Before("pt()")  // 绑定切入点与通知关系
    public void before () {  // 定义通知
        System.out.println(System.currentTimeMillis());  
    }  
}


@Configuration  
@ComponentScan("com.qxiao")  
@EnableAspectJAutoProxy  // 告知Spring用注解开发AOP
public class SpringConfig {  
}
```

AOP工作流程
1.  Spring容器启动
2.  读取所有切面配置中的切入点
3.  初始化bean, 判定bean对应的类中的方法是否匹配到任意切入点
   -  匹配失败，创建对象
   -  匹配成功，创建原始对象 ( 目标对象 ) 的代理对象
4.  获取bean执行方法
   -  获取bean，调用方法并执行，完成操作
   -  获取的bean是代理对象时，根据代理对象的运行模式运行原始方法与增强的内容，完成操作

## 切入点表达式

-  切入点： 要进行增强的方法
-  切入点表达式：要进行增强的方法的描述方式

```java
package com.qxiao.dao;

public interface BookDao {
	public void update();
}

publick class BookDaoImpl implements BookDao {
	public void update() {
		System.out.println("book dao update ...")
	}
}
```

描述方式一： 执行com.qxiao.dao包下的BookDao接口中的无参数update方法
```java
execution(void com.qxiao.dao.BookDao.update());
```

描述方式二： 执行com.qxiao.dao.impl包下的BookDaoImpl类中的无参数update方法
```java
execution(void com.qxiao.dao.impl.BookDaoImpl.update());
```

切入点表达式标准格式： 动作关键字 ( 访问修饰符 返回值 包名.类/接口名.方法名 ( 参数 ) 异常名 )
```java
execution( public User com.qxiao.service.UserService.findById (int) )
```
-  动作关键字：描述切入点的行为动作，例如excution表示执行到指定切入点
-  访问修饰符：public, private等，可以省略
-  返回值
-  包名
-  类/接口名
-  方法名
-  参数
-  异常名：方法定义中抛出指定异常，可以省略

可以使用通配符描述切入点，快速描述
-  * :  单个独立的任意符号，可以独立出现，也可以作为前缀或者后缀的匹配符出现4
```java
// 匹配com.qxiao包下的任意包中的UserService类或接口中的所有find开头的带有一个参数的方法
execution( public * com.qxiao.*.UserService.find* (*)) 
```
-  .. :  多个连续的任意符号，可以独立出现，常用于简化包名与参数的书写
```java
// 匹配com包下的任意包中UserService类或接口中所有名称为findById的方法
exection( public User com..UserService.findById(..))
```
-  +:  专用于匹配子类类型
```java
execution(* *..*Service+.* (..))
```

## 书写技巧
-  所有代码按照标准规范开发，否则以下技巧全部失效
-  描述切入点通常描述接口，而不是实现类
-  访问控制修饰符针对接口开发均采用public描述 ( 可省略访问控制修饰符描述 )
-  返回值类型对于增删改类使用精准类型加速匹配，对于查询类使用 * 通配快速描述
-  包名书写尽量不使用 .. 匹配，效率过低，常用 * 做单个包描述匹配，或精准匹配
-  接口名/类名书写名称与模块相关的采用 * 匹配，例如UserService书写成 ```*Service``` , 绑定业务层接口名
- 方法名书写以动词进行精准匹配，名词采用 * 匹配，例如getById书写成getBy* , selectAll书写成selectAll
-  参数规则较为复杂，根据业务方法灵活调整
-  通常不使用异常作为匹配规则

## AOP通知类型
-  AOP通知描述了抽取的共性功能，根据共性功能抽取的位置不同，最终运行代码时要将其加入到合理的位置
-  AOP通知共分为5种类型
    1.  前置通知
    2.  后置通知
    3.  环绕通知 ( 重点 )
    4.  返回后通知 ( 了解 )
    5.  抛出异常后通知 ( 了解 )

```java
@Repository  
public class BookDaoImpl implements BookDao {  
  
   /* public void save() {  
        System.out.println(System.currentTimeMillis());        System.out.println("book dao save ...");    }*/  
    public void update() {  
  
        System.out.println("book dao update is running ...");  
    }  
  
    public int select () {  
        System.out.println("book dao select is running ...");  
//        int i = 1/0;  
        return 100;  
    }  
}

@Component  
@Aspect  
public class MyAdvice {  
    @Pointcut("execution(void com.qxiao.dao.BookDao.update())")  
    private void pt() {}  
  
    @Pointcut("execution(int com.qxiao.dao.BookDao.select())")  
    private void selectPt() {}  
  
    @Before("pt()")  
    public void before () {  
        System.out.println("before advice ...");  
    }  
  
    @After("pt()")  
    public void after () {  
        System.out.println("after advice ...");  
    }  
  
    @Around("pt()")  
    public void around (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {  
        System.out.println("around before advice ...");  
        proceedingJoinPoint.proceed(); // 表示对原始操作的调用  
        System.out.println("around after advice ...");  
    }  
  
    @Around("selectPt()")  
    public Object aroundSelect (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {  
        System.out.println("around before advice ...");  
        Object proceed = proceedingJoinPoint.proceed();// 表示对原始操作的调用  
        System.out.println("around after advice ...");  
        return proceed;  
    }  
  
    @AfterReturning("selectPt()")  // 仅当原始操作无异常执行完成，才会执行  
    public void afterReturning () {  
        System.out.println("afterReturning advice ...");  
    }  
  
    @AfterThrowing("selectPt()")  
    public void afterThrowing () {  
        System.out.println("afterThrowing advice ...");  
    }  
}
```

@Around注意事项
1.  环绕通知必须依赖形参ProceedingJoinPoint才能实现对原始方法的调用，进而实现原始方法调用前后同时添加通知
2.  通知中如果未使用ProceedingJoinPoint对原始方法进行调用将跳过原始方法的执行
3.  对原始方法的调用可以不接收返回值，通知方法设置成void即可，如果接收返回值，必须设定为Object类型
4.  原始方法的返回值如果是void类型，通知方法的返回值类型可以设置成void, 也可以设置成Object
5.  由于无法预知原始方法运行后是否会抛出异常，因此环绕通知方法必须抛出Throwable对象

## AOP通知获取数据

获取切入点方法的参数
 -  JoinPoint: 适用于前置、后置、返回后、抛出异常后通知
 -  ProceedJointPoint: 适用于环绕通知
获取切入点方法返回值
 -  返回后通知
 -  环绕通知
获取切入点方法运行异常信息
 -  抛出异常后通知
 -  环绕通知

```java
@Before("pt()")  
public void before () {  
    System.out.println("before advice ...");  
}@Before("pt()")  
  
public void before (JoinPoint joinPoint) {  
    System.out.println("before advice ..." + joinPoint);  
}  
  
@After("pt()")  
public void after () {  
    System.out.println("after advice ...");  
}  
  
public void after (JoinPoint joinPoint) {  
    System.out.println("before advice ..." + joinPoint);  
}

@Around("pt()")  
public void around (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {  
    System.out.println("around before advice ...");  
    proceedingJoinPoint.proceed(); // 表示对原始操作的调用  
    System.out.println("around after advice ...");  
}  
  
@Around("selectPt()")  
public Object aroundSelect (ProceedingJoinPoint proceedingJoinPoint) throws Throwable {  
    System.out.println("around before advice ...");  
    Object proceed = proceedingJoinPoint.proceed();// 表示对原始操作的调用  
    System.out.println("around after advice ...");  
  
    // 如果原始操作有返回值，Around必须也要有返回值  
    return proceed;  
}

@AfterReturning("selectPt()")  // 仅当原始操作无异常执行完成，才会执行  
public void afterReturning () {  
    System.out.println("afterReturning advice ...");  
}  
  
@AfterReturning(value = "selectPt()", returning = "ret")  // 仅当原始操作无异常执行完成，才会执行  
public void afterReturning (JoinPoint joinPoint, Object ret) {  
    System.out.println(joinPoint);  
    System.out.println("afterReturning advice ..." + ret);  
}  
  
@AfterThrowing("selectPt()")  
public void afterThrowing () {  
    System.out.println("afterThrowing advice ..." );  
}  
  
@AfterThrowing(value = "selectPt()", throwing = "err")  
public void afterThrowing (JoinPoint joinPoint, Throwable err) {  
    System.out.println(joinPoint);  
    System.out.println("afterThrowing advice ..." + err);  
}
```