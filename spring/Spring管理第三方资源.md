
```xml
	<!-- DruiDataSource案列 -->
	<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">  
	    <property name="driverClassName" value="${jdbc.driver}"/>  
	    <property name="url" value="${jdbc.url}" />  
	    <property name="username" value="${jdbc.username}" />  
	    <property name="password" value="${jdbc.password}" />  
	</bean>
```

```java
ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
DataSource dataSource =(DataSource) ctx.getBean("dataSource")
```