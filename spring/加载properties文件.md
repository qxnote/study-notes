``` xml
<!-- 1、开启context命名空间
	xmlns:context="http://www.springframework.org/schema/context" 

	http://www.springframework.org/schema/context         
    http://www.springframework.org/schema/context/spring-context.xsd
-->  

<?xml version="1.0" encoding="UTF-8"?>  
<beans xmlns="http://www.springframework.org/schema/beans"  
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
       xmlns:context="http://www.springframework.org/schema/context"  
       xsi:schemaLocation="  
            http://www.springframework.org/schema/beans           
	        http://www.springframework.org/schema/beans/spring-beans.xsd           
            http://www.springframework.org/schema/context         
            http://www.springframework.org/schema/context/spring-context.xsd       
       ">  

	<!-- 2、使用context命名空间加载properties文件 -->
	<!-- 不加载系统属性（system-properties-mode="NEVER"） -->
	<context:property-placeholder 
		location="jdbc.properties" 
		system-properties-mode="NEVER"/>
	<!-- 加载多个properties文件 -->
	<context:property-placeholder location="jdbc.properties, redis.properties" />
	<!-- 加载所有properties文件 -->
	<context:property-placeholder location="*.properties" />
	<!-- 加载properties文件的标准格式 -->
	<context:property-placeholder location="classpath:*.properties" />
	<!-- 从类路径或jar包中搜索并加载properties文件 -->
	<context:property-placeholder location="classpath*:*.properties" />

	<!-- 3、使用properties文件 -->
	<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">  
	    <property name="driverClassName" value="${jdbc.driver}"/>  
	    <property name="url" value="${jdbc.url}" />  
	    <property name="username" value="${jdbc.username}" />  
	    <property name="password" value="${jdbc.password}" />  
	</bean>
</beans>
```