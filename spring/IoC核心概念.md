
# IoC Inversion of Control 控制反转

使用对象时，由主动new产生对象转换为由外部提供对象，此过程中对象的创建控制权由程序转移到外部，此思想称为控制反转

Spring技术对IoC思想进行了实现

- Spring提供了一个容器，称为IoC容器，用来充当IoC思想中的外部
- IoC容器负责对象的创建，初始化等一系列工作，被创建或被管理的对象在IoC容器中统称为Bean

目标：充分解耦

- 使用IoC容器管理bean(IoC)
- 在IoC容器内将有依赖关系的bean进行关系绑定(DI)

## IoC入门分析

1. 管理什么？ ( Service与Dao )
2. 如何将被管理的对象告知IoC容器? ( 配置 )
3. 被管理的对象交给IoC容器，如何获取到IoC容器？( 接口 )
4. IoC容器得到后，如何从容器中获取bean? ( 接口方法 )
5. 使用Spring导入哪些坐标？ ( pom.xml )


IoC入门案例

1.  导入spring-context坐标
```xml
<!-- pom.xml -->  
<dependency>  
    <groupId>org.springframework</groupId>  
    <artifactId>spring-context</artifactId>  
    <version>5.2.10.RELEASE</version>  
</dependency>
```

2.  配置bean
```xml
<!-- resources/application.xml -->
<?xml version="1.0" encoding="UTF-8"?>  
<beans xmlns="http://www.springframework.org/schema/beans"  
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
       xsi:schemaLocation="http://www.springframework.org/schema/beans    http://www.springframework.org/schema/beans/spring-beans.xsd">  
    <!--    1. 导入spring-context坐标 -->  

    <!--    2. 配置bean, 实现类   -->  
    <!--    bean标签表示配置bean-->  
    <!--    id属性给bean标识-->  
    <!--    class属性给bean定义类-->  
    <bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl"></bean>  
  
    <bean id="bookService" class="org.qxnote.service.impl.BookServiceImpl"></bean>  
</beans>
```

3.  获取IoC容器
```java
// 创建容器，加载配置文件
ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

// 获取bean
BookDao booDao = (BookDao) ctx.getBean("bookDao");  
booDao.save();
```

## DI入门案例分析

1.  基于IoC管理bean
2.  Service中使用new创建的Dao对象是否保留？( 否 )
3. Service中使用Dao对象如何进入到Service中？( 提供方法 )
4.  Service与Dao间关系如何描述？( 配置 )

配置Service与Dao的关系
```java
public class BookServiceImpl implements BookService {  
    private BookDao bookServiceDao;  
  
    public void setBookServiceDao(BookDao bookServiceDao) {  
        this.bookServiceDao = bookServiceDao;  
    }  
  
    public void save() {  
        System.out.println("book service save ...");  
        bookServiceDao.save();  
    }  
}
```

```xml
<!-- 方式一：使用构造方法实例化bean   -->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl"></bean> 
  
<bean id="bookService" class="org.qxnote.service.impl.BookServiceImpl">
	<!-- 配置Service与Dao的关系, name与Service中变量对应，ref与bean的id对应 -->  
	<property name="bookServiceDao" ref="bookDao"/>
</bean>
```

bean别名配置
```xml
<!-- 使用name配置bean的别名，可以定义多个，使用逗号(,), 分号(;), 空格分隔 -->
<bean id="bookDao" name="dao bdao" class="org.qxnote.dao.impl.BookDaoImpl"></bean>  
```

bean的作用范围
```xml
<!-- 使用scope配置bean的作用范围，默认为singleton(单例), prototype为多例-->
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl" scope="prototype"></bean>
```

适合交给容器管理的bean
-  表现层对象
-  业务层对象
-  数据层对象
-  工具对象
不适合交给容器管理的对象
-  封装实体的域对象

bean的实例化，调用的是对象的无参构造方法
```xml
<!-- 方式一：使用构造方法实例化bean   -->  
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl"></bean> 
```

使用静态方法实例化bean
```xml
<!-- 方式二：使用静态工厂实例化bean   -->  
<bean id="bookDao" class="org.qxnote.factory.BookDaoFactory" factory-method="getBookDao"></bean> 
```

使用实例工厂实例化bean
```xml
<!-- 方式三：使用实例工厂实例化bean   -->  
<bean id="bookImplDaoFactory" class="org.qxnote.factory.BookImplDaoFactory"/>  
<bean id="bookDao" factory-bean="bookImplDaoFactory" factory-method="getBookDao"></bean>
```

使用FactoryBean实例化bean
```xml
<!-- 方式四：使用FactoryBean实例化bean   -->  
<bean id="bookDao" class="org.qxnote.factory.BookDaoFactoryBean"/>
```


## bean的生命周期

1.  通过配置init-mothod和destroy-method
```java
public class BookDaoImpl implements BookDao {  
  
    public void save() {  
        System.out.println("book dao save ...");  
    }  
  
    // bean创建后对应的操作  
    public void init() {  
        System.out.println("init ...");  
    }  
  
  
    // bean销毁前对应的操作  
    public void destroy() {  
        System.out.println("destroy ...");  
    }  
}
```

```xml
<bean id="bookDao" class="org.qxnote.dao.impl.BookDaoImpl" scope="singleton" init-method="init" destroy-method="destroy"></bean>
```

2.  通过实现接口方法
```java
public class BookDaoImpl implements BookDao, InitializingBean, DisposableBean {  
  
    public void save() {  
        System.out.println("book dao save ...");  
    }  
  
    @Override  
    public void destroy() throws Exception {  
        System.out.println("init ...");  
    }  
  
    @Override  
    public void afterPropertiesSet() throws Exception {  
        System.out.println("destroy ...");  
    }  
}
```

虚拟机运行完直接退出，不给bean执行destroy的机会
```java
// ApplicationContext没有close方法，ClassPathXmlApplicationContext才有
ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml"); 
BookDao booDao = (BookDao) ctx.getBean("bookDao");  
booDao.save();  

ctx.registerShutdownHook(); // 注册关闭钩子，告知退出前先关闭容器
// ctx.close(); // 退出前关闭容器，destroy才会执行
```