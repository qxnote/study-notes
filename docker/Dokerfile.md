```
# 容器转为镜像
docker commit 容器Id 镜像名称:版本号

# 镜像压缩
docker save -o 压缩文件名称 镜像名称:版本号

# 镜像解压
docker load -i 压缩文件名称
```

