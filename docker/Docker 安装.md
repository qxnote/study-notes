
```
# yum包更新到最新版本
yum update

# yum-utils提供给yum-config-manager功能
# device-mapper-persistent-data，lvm2是devicemapper驱动依赖
yum install -y yum-utils device-mapper-persistent-data lvm2

# 设置yum源
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 安装docker
yum install -y docker-ce

# 查看docker版本，验证是否安装成功
docker -v
```

