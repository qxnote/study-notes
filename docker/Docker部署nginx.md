```
# -p 宿主机端口:容器端口

docker run -id --name nginx \
-p 80:80 \
-v /nginx/conf.d:/etc/nginx/conf.d \
-v /nginx/html:/etc/nginx/html \
-v /nginx/log:/usr/log/nginx \
nginx
```