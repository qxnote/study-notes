服务
```
systemctl start docker     # 启动

systemctl status docker    # 查看docker状态

systemctl stop docker      # 停止

systemctl restart docker   # 重启

systemctl enable docker    # 开机启动
```

镜像
```
docker images                 # 查看本地镜像
docker images -q              # 查看所有本地镜像id
docker pull                   # 拉取镜像
docker search                 # 查找远程镜像
docker rmi [image id]         # 删除本地镜像
docker rmi `docker images -q` # 删除所有本地镜像
```

![[Pasted image 20230223115232.png]]
REPOSITORY  镜像名称
TAG                版本
IMAGE ID       镜像Id
CREATED        创建时间
SIZE                大小

容器
```
docker ps                     # 查看正在运行的容器
docker ps -a                  # 查看所有的容器

创建容器常用的参数说明： -i -t -d 可以缩写为（-it -id）
创建容器命令：docker run
--name :为创建的容器命名。
-i：表示运行容器 
-t：表示容器启动后会进入其命令行。加入这个参数后，容器创建就能登录进去。即分配一个伪终端。 
-d：在run后面加上-d参数,则会创建一个守护式容器在后台运行（这样创建容器后不会自动登录容器）。
-p：表示端口映射，前者是宿主机端口，后者是容器内的映射端口。可以使用多个-p做多个端口映射
-v：表示目录映射关系（前者是宿主机目录，后者是宿主机上的目录），可以使用多个－v做多个目录或文件映射。

# -t 创建并进入容器，退出后会自动关闭（交互式容器）
docker run -it --name=[name] nginx:latest /bin/bash 

# -d 后台运行，不会进入容器，退出后不会自动关闭（守护式容器）
docker run -id --name=[name] nginx:latest /bin/bash 

# 进入容器
docker exec -it [name] /bin/bash

# 关闭容器
docker stop [name]

# 删除容器
docker rm [name|containerId]

# 删除所有容器
docker rm `docker ps -aq`

# 查看容器详细信息
docker inspect [name]

```