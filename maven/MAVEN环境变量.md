变量名：
```
MAVEN_HOME 
```

maven路径：  
```
D:\apache-maven-3.8.7
```

path -> 编辑 -> 新建

```
%MAVEN_HOME%\bin
```

maven本地仓库路径配置 
```xml
<!-- D:\apache\apache-maven-3.8.7\conf\settings.xml -->

<localRepository>D:\apache\maven\repository</localRepository>
```

远程仓库镜像配置

```xml
<!-- D:\apache\apache-maven-3.8.7\conf\settings.xml -->

<mirror>
    <id>nexus-aliyun</id>
    <mirrorOf>central</mirrorOf>
    <name>Nexus aliyun</name>
    <url>http://maven.aliyun.com/nexus/content/groups/public</url>
 </mirror>
```

maven命令

```cmd
mvn compile    # 编译
mvn clean      # 清理
mvn test       # 测试
mvn package    # 打包
mvn install    # 安装到本地仓库
```