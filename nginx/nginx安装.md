tar -zxvf nginx-1.18.0.tar.gz

1、编译./configure

2、make && make install


安装报错1：
```
checking for OS
 + Linux 3.10.0-693.el7.x86_64 x86_64
checking for C compiler ... not found

./configure: error: C compiler cc is not found
```

解决方法：
```
yum install -y gcc
```

安装报错2：
```
./configure: error: the HTTP rewrite module requires the PCRE library.
You can either disable the module by using --without-http_rewrite_module
option, or install the PCRE library into the system, or build the PCRE library
statically from the source with nginx by using --with-pcre=<path> option.
```

解决方法：
```
yum install -y pcre pcre-devel
```

安装报错3：
```
./configure: error: the HTTP gzip module requires the zlib library.
You can either disable the module by using --without-http_gzip_module
option, or install the zlib library into the system, or build the zlib library
statically from the source with nginx by using --with-zlib=<path> option.
```

解决方法：
```
yum install -y zlib zlib-devel
```

nginx 常用命令
```
nginx 启动nginx

nginx -s stop  停止nginx

nginx -s quit  更优雅的退出nginx

nginx -s reload  重载配置文件

nginx -t  测试配置文件是否有误
```

关闭防火墙
```
systemctl stop firewalld.service
```

禁止防火墙开机启动
```
systemctl disable firewalld.service
```

放行端口
```
firewall-cmd --zone=public --add-port=80/tcp --permanent
```

重启防火墙
```
firewall-cmd --reload
```