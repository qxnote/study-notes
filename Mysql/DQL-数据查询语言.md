DQL-语法

```MySql
SELECT
	字段列表
FROM
	表名列表
WHERE
	查询条件
GROUP BY
	分组字段列表
HAVING
	分组后条件列表
ORDER BY
	排序字段列表
LIMIT
	分页参数
```


DQL-基本查询

```MySQL
-- 查询多个字段
SELECT 字段1,字段2,...,字段n FROM 表名;

-- 查询所有字段
SELECT * FROM 表名;

-- 设置别名
SELECT 字段名 [AS 别名] FROM 表名;

-- 去除重复记录
SELECT DISTINCT 字段列表 FROM 表名;
```

DQL-条件查询

```MySQL
SELECT 字段列表 FROM 表名 WHERE 条件列表;
```

|比较运算符|功能|
| ---|---|
|>|大于|
|>=|大于等于|
|<|小于|
|<=|小于等于|
|=|等于|
|<> 或 !=|不等于|
|BETWEEN … AND …|在某个范围内（含最小、最大值） |
|IN(…)|在in之后的列表中的值，多选一|
| IS NULL | 是NULL |
|LIKE 占位符| 模糊匹配（_匹配单个字符，%匹配任意个字符）|

![[Pasted image 20230226195622.png]]

