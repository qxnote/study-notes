### 创建

```MySQL
-- 创建数据表
CREATE TABLE 表名 (
	字段名 字段类型 [COMMENT '字段注释'],
	...
	字段名 字段类型 [COMMENT '字段注释']

) [COMMENT '表注释'];
```

### 修改表名

```MySQL
-- 修改表名
ALTER TABLE 表名 RENAME TO 新表名;
```

### 查询

``` MySQL
-- 查询所有数据表
SHOW TABLES;

-- 查询表结构
DESC 表名;

-- 查询建表语句
SHOW CREATE TABLE 表名;
```


### 修改表字段

```MySQL
-- 添加字段
ALTER TABLE 表名 ADD 字段名 类型(长度) [COMMENT '注释'] [约束];

-- 修改数据类型
ALTER TABLE 表名 MODIFY 字段名 新数据类型(长度);

-- 修改字段名和字段类型
ALTER TABLE 表名 CHANGE 旧字段名 新字段名 类型(长度) [COMMENT '注释'] [约束];
```


### 删除表字段

```MySQL
-- 删除字段
ALTER TABLE 表名 DROP 字段名;
```


### 删除表

```MySQL
-- 删除表
DROP TABLE [IF EXISTS] 表名;

-- 删除指定表，并重新创建该表
TRUNCATE TABLE 表名;
```
