### 创建

``` MySQL
-- utf8 是3个字节，特殊字符（如表情），占4个字节，所以推荐使用utf8mb4;
CREATE DATABASE [IF NOT EXISTS] 数据库名 [DEFAULT CHARTSET 字符集] [COLLATE 排序规则]
-- 例：
CREATE DATABASE IF NOT EXISTS db_name DEFAULT CHARTSET utf8mb4
```


### 查询

``` MySQL 
-- 查询所有数据库
SHOW DATABASES;

-- 查询当前数据库
SELECT DATABASE();
```


### 使用

``` MySQL
-- 使用db_name数据库
USE db_name;
```


### 删除
```MySQL
-- 删除数据库
DROP DATABASE [IF EXISTS] db_name;
```