常用配置选项

| 配置选项 | 描述 | 默认值 | 建议值 |
| -- | -- | -- | -- |
| CMAKE_INSTALL_PREFIX | 安装基目录(basedir) | /usr/local/mysql | 根据需求 |
| MYSQL_DATADIR | 数据目录 | | 根据需求 |
| SYSCONFDIR | 默认配置文件my.cnf路径 | | /etc |
| MYSQL_TCP_PORT | TCP/IP端口 | 3306 | 非默认端口 |
| MYSQL_UNIX_ADDR | socket文件路径 | /tmp/mysql.sock | basedir |
| DEFAULT_CHARSET  | 默认字符集 | latin1 | urf8mb4 |
| DEFAULT_COLLATION | 默认校验规则 | latin1_swedish_ci | utf8mb4_general_ci |
| WITH_EXTRA_CHARSETS | 扩展字符集 | all | all |
| ENABLED_LOCAL_INFILE | 是否启用本地加载外部数据文件功能 | OFF | 建议开启 |

存储引擎相关配置项

```
选项值为布尔值，0代表不编译到服务器中，1代表编译，建议都编译到服务器中。其他存储引擎可根据需要在安装时通过WITH_XXX_STORAGE_ENGINE=1的方式编译到服务器中。

WITH_INNOBASE_STORAGE_ENGINE       InnoDB存储引擎
WITH_PARTITION_STORAGE_ENGINE      是否支持分区
WITH_FEDERATED_STORAGE_ENGINE      本地数据库是否可以访问远程mysql
WITH_BLACKHOLE_STORAGE_ENGINE      黑洞存储引擎，接收数据，但不存储，直接丢弃
WITH_MYISAM_STORAGE_ENGINE         MYISAM存储引擎
```

安装相应的依赖包

```
yum -y install ncurses ncurses-devel cmake
```

安装配置

``` sh
#!/bin/bash
cmake . \
-DWITH_BOOST=/usr/local/boost_1_59_0 \
-DCMAKE_INSTALL_PREFIX=/usr/local/mysql5.6 \
-DMYSQL_DATADIR=/usr/local/mysql5.6/data \
-DMYSQL_TCP_PORT=3307 \
-DMYSQL_UNIX_ADDR=/usr/local/mysql5.6/mysql5.6.sock \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_PARTITION_STORAGE_ENGINE=1 \
-DWITH_FEDERATED_STORAGE_ENGINE=1 \
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
-DWITH_MYISAM_STORAGE_ENGINE=1 \
-DENABLED_LOCAL_INFILE=1 \
-DWITH_EXTRA_CHARSETS=all \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_general_ci
```

chmod +x cmake.sh

报错1：

```
Cannot find appropriate system libraries for WITH_SSL=system.
Make sure you have specified a supported SSL version. 
Valid options are : 
system (use the OS openssl library), 
yes (synonym for system), 
</path/to/custom/openssl/installation>
```

解决：

```
 yum install -y openssl-devel
```

报错2：
```
CMake Error: your CXX compiler: "CMAKE_CXX_COMPILER-NOTFOUND" was not found.   Please set CMAKE_CXX_COMPILER to a valid compiler path or name.
```

```
yum install -y gcc gcc-c++
```

编译安装
make && make install

1. 进入安装目录
- 修改目录权限
```shell
chown -R mysql.mysql /usr/local/mysql5.6
ll -d  /usr/local/mysql5.6
```

- 初始化数据库
```
scripts/mysql_install_db --user=mysql
```

-  查看数据目录是否有数据文件
```
ls data/
```

2. 拷贝启动脚本到/etc/init.d/目录
```
cp support-files/mysql.server /etc/init.d/mysql5.6
```

3. 启动数据库
```
service mysql5.6 start
```

报错： 
[root@localhost scripts]# ./mysql_install_db --user=mysql
bash: ./mysql_install_db: /usr/bin/perl: 坏的解释器: 没有那个文件或目录

解决： yum -y install perl perl-devel

报错：
[root@localhost scripts]# ./mysql_install_db --user=mysql
FATAL ERROR: please install the following Perl modules before executing ./mysql_install_db:
Data::Dumper


创建用户和组

groupadd mysql

useradd -r -g mysql

